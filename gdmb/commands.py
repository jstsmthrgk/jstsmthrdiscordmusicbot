from .client import Client, add_slash_command
from nextcord import SlashOption, Interaction
import nextcord

@add_slash_command(
    Client, 
    "Stoppt die Wiedergabe", 
    lambda v: ()
)
async def stop(interaction: Interaction):
    if interaction.guild.voice_client is not None:
        if interaction.guild.id in interaction.client.status_messages:
            await interaction.client.status_messages[interaction.guild.id].delete()
            interaction.client.status_messages.pop(interaction.guild.id)
        await interaction.guild.voice_client.disconnect()
        await interaction.send(content="Ich stoppe.", ephemeral=True)

@add_slash_command(
    Client,
    "Spielt Radio live",
    lambda v: (
        SlashOption(
            name="sender",
            required=True,
            choices=dict(zip([i[0] for i in v.stations], range(len(v.stations))))
        ),
    )
)
async def radio(interaction: Interaction, station: int = None):
    if await interaction.client.connect_to_vc(interaction.user, interaction.guild.voice_client):
        source = nextcord.FFmpegOpusAudio(interaction.client.stations[station][1])
        interaction.guild.voice_client.stop()
        interaction.guild.voice_client.play(source)
        await interaction.client.show_playing(interaction.guild,(name := interaction.client.stations[station][0]))
        await interaction.response.send_message(f"Ich spiele jetzt {name}.", ephemeral=True, delete_after=3)
    else:
        await interaction.response.send_message("Du bist in keinem Voicechannel", ephemeral=True, delete_after=3)

@add_slash_command(
    Client,
    "Spielt unendliche Loops",
    lambda v: (
        SlashOption(
            name="song",
            required=True,
            choices=dict(zip([i[0] for i in v.loops], range(len(v.loops))))
        ),
    )
)
async def loop(interaction: Interaction, song: int = None):
    if await interaction.client.connect_to_vc(interaction.user,interaction.guild.voice_client):
        source = nextcord.FFmpegOpusAudio(interaction.client.loops[song][1], before_options="-stream_loop -1", codec="copy")
        interaction.guild.voice_client.stop()
        interaction.guild.voice_client.play(source)
        await interaction.client.show_playing(interaction.guild,(name := interaction.client.loops[song][0]))
        await interaction.response.send_message(f"Ich spiele jetzt {name}.", ephemeral=True, delete_after=3)
    else:
        await interaction.response.send_message("Du bist in keinem Voicechannel", ephemeral=True, delete_after=3)

noise_colors = [
    ("White", "white"),
    ("Pink", "pink"),
    ("Brown", "brown"),
    ("Blue", "blue"),
    ("Violet", "violet"),
    ("Velvet", "velvet")
]

@add_slash_command(
    Client,
    "Spielt Rauschen",
    lambda v: (
        SlashOption(
            name="color",
            required=True,
            choices=dict(zip([i[0] for i in noise_colors], range(len(noise_colors))))
        ),
    )
)
async def noise(interaction: Interaction, color: int = None):
    if await interaction.client.connect_to_vc(interaction.user, interaction.guild.voice_client):
        source = nextcord.FFmpegOpusAudio(f"anoisesrc=c={noise_colors[color][1]}", before_options="-f lavfi")
        interaction.guild.voice_client.stop()
        interaction.guild.voice_client.play(source)
        await interaction.client.show_playing(interaction.guild,(name := noise_colors[color][0] + " Noise"))
        await interaction.response.send_message(f"Ich spiele jetzt {name}.", ephemeral=True, delete_after=3)
    else:
        await interaction.response.send_message("Du bist in keinem Voicechannel", ephemeral=True, delete_after=3)

## This doesn't quite work, so disable it
#@add_slash_command(
#    Client,
#    "Spielt alles, was youtube-dl schafft",
#    lambda v: ()
#)
#async def play(interaction: Interaction, url: str):
#    if await interaction.client.connect_to_vc(interaction.user,interaction.guild.voice_client):
#        data = await interaction.client.loop.run_in_executor(None, lambda: interaction.client.ytdl.extract_info(url, download=False))
#        if data is not None:
#            if "entries" in data:
#                data = data["entries"][0]
#            source = nextcord.FFmpegOpusAudio(data["url"])
#            interaction.guild.voice_client.stop()
#            interaction.guild.voice_client.play(source)
#            await interaction.client.show_playing(interaction.guild,data["title"])
#            await interaction.response.send_message(f"Ich spiele jetzt {data['title']}.", ephemeral=True, delete_after=3)
#        else:
#            await interaction.response.send_message("Ich kann mit der URL nicht umgehen.", ephemeral=True, delete_after=3)
#    else:
#        await interaction.response.send_message("Du bist in keinem Voicechannel", ephemeral=True, delete_after=3)
